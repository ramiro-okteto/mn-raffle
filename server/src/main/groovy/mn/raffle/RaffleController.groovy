package mn.raffle


import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.rules.SecurityRule
import io.reactivex.Single

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller('/api/v1/raffle')
class RaffleController {

    RaffleService raffleService

    RaffleController(RaffleService raffleService){
        this.raffleService = raffleService
    }

    @Post("/participants")
    Single<List<Participant>> participants(String sheetId, String tabId) {
        Single.create { emitter ->
            List<Participant> list = raffleService.loadParticipants(sheetId, tabId)
            emitter.onSuccess(list)
        }
    }

    @Post("/prizes")
    Single<List<Participant>> prizes(String sheetId, String tabId) {
        Single.create { emitter ->
            List<Participant> list = raffleService.loadPrizes(sheetId, tabId)
            emitter.onSuccess(list)
        }
    }

    @Post("/winner")
    Single<Boolean> winner(String sheetId, String tabId, String prize, String winner) {
        Single.create { emitter ->
            emitter.onSuccess(raffleService.winner(sheetId, tabId, prize, winner))
        }
    }

    @Post("/notpressent")
    Single<Boolean> notPresent(String sheetId, String tabId, String winner) {
        Single.create { emitter ->
            emitter.onSuccess(raffleService.notPresent(sheetId, tabId, winner))
        }
    }
}
