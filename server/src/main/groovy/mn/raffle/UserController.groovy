package mn.raffle

import io.micronaut.http.annotation.Controller
import io.micronaut.http.annotation.Get
import io.micronaut.http.annotation.Post
import io.micronaut.security.annotation.Secured
import io.micronaut.security.authentication.Authentication
import io.micronaut.security.rules.SecurityRule
import io.reactivex.Single

@Secured(SecurityRule.IS_AUTHENTICATED)
@Controller('/api/v1/user')
class UserController {

    @Get
    UserDetail authenticated(Authentication authentication) {
        new UserDetail(
                name: authentication.attributes['name'],
                email: authentication.attributes['email'],
                picture: authentication.attributes['picture']
        )
    }

}
