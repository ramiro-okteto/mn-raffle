import { Module, GetterTree, MutationTree } from 'vuex';

interface LoaderState {
    loading: number;
}

const state: LoaderState = {
    loading: 0,
};

const getters: GetterTree<LoaderState, any> = {
    loading: state => state.loading,
};

const mutations: MutationTree<LoaderState> = {
    START_LOADING: state => state.loading++,
    FINISH_LOADING: state => state.loading--,
};

const namespaced = true;

const loader: Module<LoaderState, any> = {
    namespaced,
    state,
    getters,
    mutations,
};

export default loader;