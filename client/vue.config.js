const path = require('path')

module.exports = {
    chainWebpack: config => {
        const apiClient = process.env.VUE_APP_API_CLIENT // mock or server
        config.resolve.alias.set(
            '@services',
            path.resolve(__dirname, `src/services/${apiClient}`)
        )
    }
}